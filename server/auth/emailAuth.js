const Joi = require('joi');

const registerSchema = Joi.object({
    firstName: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    lastName: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    books: Joi.array(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .min(5)
        .required(),

    repeatPassword: Joi.any().valid(Joi.ref('password'))
    .required(),

    email: Joi.string()
        .email()
        .lowercase()
        .required()

})

module.exports = registerSchema;