const express = require("express");
const app = express();
const PORT = 3001;
const User = require("./models/User");
const Book = require("./models/Book");
const cors = require("cors");
const registerSchema = require("./auth/emailAuth");
const loginSchema = require("./auth/loginAuth");
const Joi = require("joi");

const mongoose = require("mongoose");
const morgan = require("morgan");

const MONGODB_URI =
  "mongodb+srv://Seba:Test123@test.b3oyu.mongodb.net/library?retryWrites=true&w=majority";

app.use(morgan("tiny"));
app.use(express.json());
app.use(cors());

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on("connected", () => {
  console.log("Mongoose is connected");
});

app.post("/api/create", async (req, res) => {
  try {
    const result = await registerSchema.validateAsync(req.body);
    console.log("All parameters within the requirements", result);

    const createUser = new User({
      firstName: result.firstName,
      lastName: result.lastName,
      password: result.password,
      email: result.email,
    });

    console.log("User created successfull", createUser);
    await createUser.save();
    res.sendStatus(200);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.post("/api/login", async (req, res) => {
  try {
    const validatedUser = await loginSchema.validateAsync(req.body);
    console.log("Validated User : ", validatedUser);
    const user = await User.findOne({ email: validatedUser.email }).exec();
    const userFrontInfo = {
      id: user._id,
      firstName: user.firstName,
      lastName: user.lastName,
      books: user.books,
      password: user.password,
      email: user.email,
    };
    console.log("Found user", user);
    if (user !== null) {
      if (validatedUser.password === user.password) {
        console.log("Password OK");
        res.send(userFrontInfo);
      } else {
        console.log("Password not OK");
        res.sendStatus(400);
      }
    }
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.get("/api/allbooks", async (req, res) => {
  try {
    const allBooks = await Book.find({});
    res.send(allBooks);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.put("/api/borrowbook", async (req, res) => {
  try {
    let borrowedBook = await Book.findById(req.body.bookId);
    const loggedUser = await User.findById(req.body.userId);
    if (loggedUser.books.length < 4) {
      if (borrowedBook.counter > 0) {
        await loggedUser.books.push(borrowedBook._id);
        await --borrowedBook.counter;
        await loggedUser.save();
        await borrowedBook.save();
        console.log(loggedUser);
      }
    }
    res.send(loggedUser.books);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.post("/api/mybooks", async (req, res) => {
  try {
    const user = await User.findById(req.body.userId);
    const usersBooksIds = user.books;
    const usersBooks = await Book.find().where("_id").in(usersBooksIds).exec();
    res.send(usersBooks);
    console.log(usersBooks);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.post("/api/getUser", async (req, res) => {
  try {
    const user = await User.findById(req.body.userId);
    res.send(user);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.post("/api/editUser", async (req, res) => {
  try {
    let user = await User.findById(req.body.userId);
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.email = req.body.email;
    user.password = req.body.password;
    const modifiedUser = await user.save();
    // console.log(modifiedUser)
    res.send(200);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.post("/api/search", async (req, res) => {
  try {
    const searchedTerm = req.body.search;
    const searchedBooks = await Book.find({
      title: { $regex: searchedTerm, $options: "i" },
    });
    res.send(searchedBooks);
    // console.log(searchedBooks);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});

app.listen(PORT, console.log(`Server is starting at port ${PORT}`));
