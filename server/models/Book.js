const mongoose = require("mongoose");

const BookSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  counter: {
    type: Number,
    required: true,
  },
  genre: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.models.Book || mongoose.model("Book", BookSchema);
