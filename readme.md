MyLibrary is a beginner MongoDB, Express, React, Node project.

To run : open the client folder and run the commend "npm start" and open the server folder and run the comment "npm run dev"

The whole idea of the project was to combine front-end and back-end skills to create a simple but functional web app.