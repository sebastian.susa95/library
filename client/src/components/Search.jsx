import React, { useContext, useState } from "react";
import { BsSearch } from "react-icons/bs";
import { UserContext } from "../userContext";
import Axios from "axios";
import { useHistory } from "react-router-dom";

export default function Search() {
  const [search, setSearch] = useState("");
  const { foundBooks, setFoundBooks, user } = useContext(UserContext);
  let history = useHistory();
  const searchBooks = () => {
    console.log(search);
    Axios.post("http://localhost:3001/api/search", {
      search: search,
    })
      .then((response) => {
        setFoundBooks(response.data);
        history.push("/allbooks");
        // console.log(foundBooks);
      })
      .catch((err) => console.log(err));
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      searchBooks();
    }
  };

  const searchBar = (
    <div className="bg-white flex items-center rounded-full shadow-xl h-10 mt-2">
      <input
        onKeyPress={handleKeyPress}
        onChange={(e) => setSearch(e.target.value)}
        className="rounded-l-full w-full py-4 px-6 text-gray-700 leading-tight focus:outline-none h-10"
        id="search"
        type="text"
        placeholder="Search"
      />
      <div className="p-2 flex">
        <button onClick={() => searchBooks()}>
          <BsSearch className="bg-purple-600 text-white rounded-full p-2 hover:bg-purple-400 focus:outline-none w-8 h-8 flex items-center justify-center" />
        </button>
      </div>
    </div>
  );

  return <>{user ? searchBar : null}</>;
}
