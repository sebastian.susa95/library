import React from 'react';
    
export default function Home(){
    return(
       <div className="grid">
           <p className="text-center m-6 text-6xl">Welcome to MyLibrary</p>
           <p className="text-center m-6 text-xl">MyLibrary is a beginner react / node.js project, to learn and develop. Enjoy!</p>

       </div> 
    )
}