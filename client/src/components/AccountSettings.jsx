import React, { useState, useContext, useEffect } from "react";
import Axios from "axios";
import { UserContext } from "../userContext";
import { FaRegEdit, FaRegSave } from "react-icons/fa";
import { ImCancelCircle } from "react-icons/im";

export default function AccountSettings() {
  const { user, setUser } = useContext(UserContext);
  const [userPass, setUserPass] = useState({});
  const [editButton, setEditButton] = useState(0);

  useEffect(() => {
    Axios.post("http://localhost:3001/api/getUser", {
      userId: user.id,
    })
      .then((res) => {
        setUserPass(res.data.password);
      })
      .catch((err) => console.log(err));
  }, []);

  const editUser = () => {
    Axios.post("http://localhost:3001/api/editUser", {
      userId: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      password: userPass,
      email: user.email,
    })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  return (
    <div className="flex justify-center">
      <div className="flex flex-col justify-center m-4 border-4 border-green-300 rounded-2xl p-4 shadow-lg bg-white">
        {/* Start First Name */}
        <div className="flex flex-row justify-start">
          <div className="m-2 text-xl text-gray-500">First Name :</div>
          <div className="">
            {editButton === 1 ? (
              <input
                className="m-2 text-xl text-purple-700"
                onChange={(e) => {
                  user.firstName = e.target.value;
                  setUser(user);
                }}
                type="text"
                placeholder="First Name"
              />
            ) : (
              <div className="m-2 text-xl text-purple-700">
                {user.firstName}{" "}
              </div>
            )}
          </div>
          <div className="m-2 text-2xl">
            {editButton === 1 ? (
              <div className="">
                <button
                  className="mr-2 text-green-500"
                  onClick={() => {
                    setEditButton(0);
                    editUser();
                  }}
                >
                  <FaRegSave />
                </button>
                <button
                  className="text-red-500"
                  onClick={() => setEditButton(0)}
                >
                  <ImCancelCircle />
                </button>
              </div>
            ) : (
              <button className="m-2 text-2xl" onClick={() => setEditButton(1)}>
                <FaRegEdit />
              </button>
            )}
          </div>
        </div>
        {/* Start Last Name */}
        <div className="flex flex-row justify-start">
          <div className="m-2 text-xl text-gray-500">Last Name :</div>
          <div className="">
            {editButton === 2 ? (
              <input
                className="m-2 text-xl text-purple-700"
                onChange={(e) => {
                  user.lastName = e.target.value;
                  setUser(user);
                }}
                type="text"
                placeholder="Last Name"
              />
            ) : (
              <div className="m-2 text-xl text-purple-700">
                {user.lastName}{" "}
              </div>
            )}
          </div>
          <div className="m-2 text-2xl">
            {editButton === 2 ? (
              <div className="">
                <button
                  className="mr-2 text-green-500"
                  onClick={() => {
                    setEditButton(0);
                    editUser();
                  }}
                >
                  <FaRegSave />
                </button>
                <button
                  className="text-red-500"
                  onClick={() => setEditButton(0)}
                >
                  <ImCancelCircle />
                </button>
              </div>
            ) : (
              <button className="m-2 text-2xl" onClick={() => setEditButton(2)}>
                <FaRegEdit />
              </button>
            )}
          </div>
        </div>
        {/* Start Email */}
        <div className="flex flex-row justify-start">
          <div className="m-2 text-xl text-gray-500">E-mail :</div>
          <div className="">
            {editButton === 3 ? (
              <input
                className="m-2 text-xl text-purple-700"
                onChange={(e) => {
                  user.email = e.target.value;
                  setUser(user);
                }}
                type="text"
                placeholder="Last Name"
              />
            ) : (
              <div className="m-2 text-xl text-purple-700">{user.email}</div>
            )}
          </div>
          <div className="m-2 text-2xl">
            {editButton === 3 ? (
              <div className="">
                <button
                  className="mr-2 text-green-500"
                  onClick={() => {
                    setEditButton(0);
                    editUser();
                  }}
                >
                  <FaRegSave />
                </button>
                <button
                  className="text-red-500"
                  onClick={() => setEditButton(0)}
                >
                  <ImCancelCircle />
                </button>
              </div>
            ) : (
              <button className="m-2 text-2xl" onClick={() => setEditButton(3)}>
                <FaRegEdit />
              </button>
            )}
          </div>
        </div>
        {/* Start Password */}
        <div className="flex flex-row justify-start">
          <div className="m-2 text-xl text-gray-500">Password :</div>
          <div className="">
            {editButton === 4 ? (
              <input
                className="m-2 text-xl text-purple-700"
                onChange={(e) => {
                  setUserPass(e.target.value);
                  setUser(user);
                }}
                type="password"
                placeholder="**************"
              />
            ) : (
              <div className="m-2 text-xl text-purple-700">**************</div>
            )}
          </div>
          <div className="m-2 text-2xl">
            {editButton === 4 ? (
              <div className="">
                <button
                  className="mr-2 text-green-500"
                  onClick={() => {
                    setEditButton(0);
                    editUser();
                  }}
                >
                  <FaRegSave />
                </button>
                <button
                  className="text-red-500"
                  onClick={() => setEditButton(0)}
                >
                  <ImCancelCircle />
                </button>
              </div>
            ) : (
              <button className="m-2 text-2xl" onClick={() => setEditButton(4)}>
                <FaRegEdit />
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
