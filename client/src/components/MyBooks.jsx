import React, { useContext, useState, useEffect } from "react";
import Axios from "axios";
import { UserContext } from "../userContext";

export default function MyBooks() {
  const { user } = useContext(UserContext);
  const [books, setBooks] = useState([]);

  useEffect(() => {
    Axios.post("http://localhost:3001/api/mybooks", {
      userId: user.id,
    })
      .then((response) => {
        setBooks(response.data);
        // console.log(response.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const bookList = books.map((book) => (
    <div
      key={book.id}
      className="max-w-sm rounded overflow-hidden shadow-lg w-72 m-auto mt-2"
    >
      <img
        className="w-full h-76 bg-gray-500"
        src={book.img}
        alt={book.title}
      />
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{book.title}</div>
        <p className="text-gray-700 text-base">{book.author}</p>
      </div>
    </div>
  ));

  return (
    <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-4 justify-items-center">
      {bookList}
    </div>
  );
}
