import React from "react";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import Axios from "axios";
import { FaRegSmileBeam } from "react-icons/fa";

export default function Register() {
  let [isOpen, setIsOpen] = useState(false);
  let [registerAnimation, setRegisterAnimation] = useState(false);

  //Setting the typed values to variables
  let [firstName, setFirstName] = useState("");
  let [lastName, setLastName] = useState("");
  let [password, setPassword] = useState("");
  let [repeatPassword, setRepeatPassword] = useState("");
  let [email, setEmail] = useState("");

  // Frontend validation
  let [firstNameError, setFirstNameError] = useState(false);
  let [lastNameError, setLastNameError] = useState(false);
  let [emailError, setEmailError] = useState(false);
  let [passwordError, setPasswordError] = useState(false);
  let [repeatPasswordError, setRepeatPasswordError] = useState(false);

  const createUser = () => {
    // Frontend validation
    if (firstName.length < 3) {
      setFirstNameError(true);
    } else {
      setFirstNameError(false);
    }

    if (lastName.length < 3) {
      setLastNameError(true);
    } else {
      setLastNameError(false);
    }

    if (!email.includes("@")) {
      setEmailError(true);
    } else {
      setEmailError(false);
    }

    if (password.length < 5) {
      setPasswordError(true);
    } else {
      setPasswordError(false);
    }

    if (password !== repeatPassword) {
      setRepeatPasswordError(true);
    } else {
      setRepeatPasswordError(false);
    }

    //Server will create the user
    Axios.post("http://localhost:3001/api/create", {
      firstName: firstName,
      lastName: lastName,
      password: password,
      repeatPassword: repeatPassword,
      email: email,
    })
      .then((response) => console.log(response))
      .then(() => {
        setRegisterAnimation(true);
      })
      .catch((err) => console.log(err));
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  const openModal = () => {
    setIsOpen(true);
  };

  return (
    <>
      <button
        onClick={openModal}
        className="m-2 mr-8 py-1 px-1 md:px-3 text-white bg-purple-400  border-2 border-purple-600 rounded-md hover:bg-purple-300"
      >
        Register
      </button>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              {!registerAnimation ? (
                <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                  <form className="bg-white rounded px-8 pt-6 pb-8 mb-4">
                    <div className="mb-4">
                      <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="username"
                      >
                        First Name
                      </label>
                      <input
                        onChange={(e) => {
                          setFirstName(e.target.value);
                        }}
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="fistname"
                        type="text"
                        placeholder="First Name"
                      />
                      {firstNameError ? (
                        <p className="text-center text-sm text-red-500 py-2">
                          First name must contain at least 3 letters
                        </p>
                      ) : null}
                    </div>

                    <div className="mb-4">
                      <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="username"
                      >
                        Last Name
                      </label>
                      <input
                        onChange={(e) => {
                          setLastName(e.target.value);
                        }}
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="lastname"
                        type="text"
                        placeholder="Last Name"
                      />
                      {lastNameError ? (
                        <p className="text-center text-sm text-red-500 py-2">
                          Last name must contain at least 3 letters
                        </p>
                      ) : null}
                    </div>
                    <div className="mb-4">
                      <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="username"
                      >
                        Email Adress
                      </label>
                      <input
                        onChange={(e) => {
                          setEmail(e.target.value);
                        }}
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="emailadres"
                        type="text"
                        placeholder="example@example.com"
                      />
                      {emailError ? (
                        <p className="text-center text-sm text-red-500 py-2">
                          Please enter a valid Email Address
                        </p>
                      ) : null}
                    </div>
                    <div className="mb-6">
                      <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="password"
                      >
                        Password
                      </label>
                      <input
                        onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        id="password"
                        type="password"
                        placeholder="******************"
                      />
                      {passwordError ? (
                        <p className="text-center text-sm text-red-500 py-2">
                          Your password must contain at least 5 symobols
                        </p>
                      ) : null}
                    </div>
                    <div className="mb-6">
                      <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="password"
                      >
                        Repeat your password
                      </label>
                      <input
                        onChange={(e) => {
                          setRepeatPassword(e.target.value);
                        }}
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        id="repeatPassword"
                        type="password"
                        placeholder="******************"
                      />
                      {repeatPasswordError ? (
                        <p className="text-center text-sm text-red-500 py-2">
                          Passwords do not match
                        </p>
                      ) : null}
                    </div>
                    <div className="flex items-center justify-center">
                      <button
                        onClick={createUser}
                        className="bg-purple-600 hover:bg-purple-400 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        type="button"
                      >
                        Register
                      </button>
                    </div>
                  </form>
                </div>
              ) : (
                <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-center align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                  <FaRegSmileBeam className="h-40 w-40 text-green-500 mx-auto my-8" />

                  <button
                    className="m-2 py-1 px-1 md:px-3 text-white bg-purple-400 align-middle border-2 border-purple-600 rounded-md hover:bg-purple-300"
                    onClick={() => {
                      setIsOpen(false);
                      setRegisterAnimation(false);
                    }}
                  >
                    Done!
                  </button>
                </div>
              )}
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
