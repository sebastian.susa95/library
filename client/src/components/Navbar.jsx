import React, { useContext } from "react";
import Login from "./Login";
import Register from "./Register";
import ProfileButton from "./ProfileButton";
import Search from "./Search";
import { UserContext } from "../userContext";
import { Link } from "react-router-dom";

function Navbar() {
  const { user } = useContext(UserContext);

  return (
    <div className="w-screen h-25 bg-green-300 flex justify-between">
      <Link to="/" className="m-4 inline-block font-mono text-lg">
        MyLibrary
      </Link>

      <Search/>
        

      <div className="flex">
        {user === null ? (
          <>
            <Login />
            <Register />
          </>
        ) : (
          <ProfileButton />
        )}
      </div>
    </div>
  );
}

export default Navbar;
