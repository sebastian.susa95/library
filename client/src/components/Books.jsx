import React, { useContext, useState, useEffect } from "react";
import Axios from "axios";
import { Popover } from "@headlessui/react";
import { UserContext } from "../userContext";

function Books() {
  const [books, setBooks] = useState([]);
  const { user, setUser, foundBooks } = useContext(UserContext);
  const [userBooks, setUserBooks] = useState(user.books);

  useEffect(() => {
    Axios.get("http://localhost:3001/api/allbooks")
      .then((response) => {
        setBooks(response.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const borrowBook = (bookID) => {
    Axios.put("http://localhost:3001/api/borrowbook", {
      userId: user.id,
      bookId: bookID,
    })
      .then((response) => setUserBooks(response.data))
      .then(() => console.log(userBooks))
      .catch((err) => console.log(err));
  };

  const bookList = books.map((book) => (
    <div
      key={book.id}
      className="max-w-sm rounded overflow-hidden shadow-lg w-72 m-auto mt-2"
    >
      <img
        className="w-full h-76 bg-gray-500"
        src={book.img}
        alt={book.title}
      />
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{book.title}</div>
        <p className="text-gray-700 text-base font-mono">{book.author}</p>
        <button className="border-2 inline-block rounded-2xl p-1 border-gray-600 bg-gray-300">
          {book.genre}
        </button>
        <p className="text-gray-700 text-base font-mono">
          Books left : {book.counter}
        </p>
        <Popover className="text-center">
          <Popover.Button className="bg-green-300 p-2 rounded-lg text-xl w-36 mt-2">
            Borrow
          </Popover.Button>
          <Popover.Panel className="absolute border-2 shadow-xl bg-white rounded-2xl w-60">
            <div className="">
              <p className="text-center text-xl m-2">
                Are you sure you want to borrow this book ?
              </p>
              <div className="text-center">
                <button
                  onClick={() => {
                    borrowBook(book._id);
                  }}
                  className="bg-green-300 p-2 rounded-lg text-lg m-2"
                >
                  <Popover.Button>Yes I'am sure</Popover.Button>
                </button>
              </div>
            </div>
          </Popover.Panel>
        </Popover>
      </div>
    </div>
  ));

  const foundBooksList = foundBooks.map((book) => (
    <div
      key={book.id}
      className="max-w-sm rounded overflow-hidden shadow-lg w-72 m-auto mt-2"
    >
      <img
        className="w-full h-76 bg-gray-500"
        src={book.img}
        alt={book.title}
      />
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{book.title}</div>
        <p className="text-gray-700 text-base">{book.author}</p>
        <button className="border-2 inline-block rounded-2xl p-1 border-gray-600 bg-gray-300">
          {book.genre}
        </button>
        <p className="text-gray-700 text-base font-mono">
          Books left : {book.counter}
        </p>
        <Popover className="text-center">
          <Popover.Button className="bg-green-300 p-2 rounded-lg text-xl w-36 mt-2">
            Borrow
          </Popover.Button>
          <Popover.Panel className="absolute border-2 shadow-xl bg-white rounded-2xl w-60">
            <div className="">
              <p className="text-center text-xl m-2">
                Are you sure you want to borrow this book ?
              </p>
              <div className="text-center">
                <button
                  onClick={() => borrowBook(book._id)}
                  className="bg-green-300 p-2 rounded-lg text-lg m-2"
                >
                  <Popover.Button>Yes I'am sure</Popover.Button>
                </button>
              </div>
            </div>
          </Popover.Panel>
        </Popover>
      </div>
    </div>
  ));

  return (
    <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-2 justify-items-center">
      {foundBooks.length === 0 ? bookList : foundBooksList}
    </div>
  );
}
export default Books;
