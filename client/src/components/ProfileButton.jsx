import React, { Fragment, useContext } from "react";
import { CgProfile } from "react-icons/cg";
import { FaPowerOff } from "react-icons/fa";
import { Menu, Transition } from "@headlessui/react";
import { Link } from "react-router-dom";
import { UserContext } from "../userContext";

function ProfileButton() {
  const { user, setUser } = useContext(UserContext);

  return (
    <Menu
      as="div"
      className="inline-block text-left mr-3 ml-2 md:mr-8 my-auto pt-1"
    >
      <Menu.Button>
        <CgProfile className="h-10 w-10 text-purple-600" />
      </Menu.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-200"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-150"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="bg-green-300 rounded-lg mx-2 py-1 absolute grid border-2 border-purple-600 right-0 px-6">
          <Menu.Item className="grid justify-center">
            <p>
              <CgProfile className="h-10 w-10 text-purple-600 mx-2 mt-2" />
            </p>
          </Menu.Item>
          <Menu.Item className="grid justify-center text-black font-mono pb-1">
            <p>Hello {user.firstName}!</p>
          </Menu.Item>
          <Menu.Item>
            {({ active }) => (
              <Link
                to="/allbooks"
                className={`${
                  active ? "text-purple-600" : "text-black"
                } text-black font-mono py-1`}
              >
                All Books
              </Link>
            )}
          </Menu.Item>
          <Menu.Item>
            {({ active }) => (
              <Link
                to="/mybooks"
                className={`${
                  active ? "text-purple-600" : "text-black"
                } text-black font-mono py-1`}
              >
                My Books
              </Link>
            )}
          </Menu.Item>
          <Menu.Item>
            {({ active }) => (
              <Link
                to="/account"
                className={`${
                  active ? "text-purple-600" : "text-black"
                } text-black font-mono py-1`}
              >
                Account settings
              </Link>
            )}
          </Menu.Item>
          <Menu.Item className="grid justify-center">
            <button onClick={() => setUser(null)}>
              <FaPowerOff className="h-8 w-8 text-red-600 m-2 hover:bg-red-300 hover:shadow-lg rounded-full" />
            </button>
          </Menu.Item>
        </Menu.Items>
      </Transition>
    </Menu>
  );
}

export default ProfileButton;
