import React, { useState } from "react";
import "./index.css";
import Navbar from "./components/Navbar";
import Books from "./components/Books";
import MyBooks from "./components/MyBooks";
import Home from "./components/Home";
import AccountSettings from "./components/AccountSettings";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { UserContext } from "./userContext";
import PrivateRoute from "./components/PrivateRoute";

function App() {
  let [user, setUser] = useState(null);
  let [foundBooks , setFoundBooks] = useState([])

  return (
    <Router>
      <UserContext.Provider value={{ user, setUser, foundBooks , setFoundBooks }}>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home}/>
          <PrivateRoute path="/allbooks" component={Books} />
          <PrivateRoute path="/mybooks" component={MyBooks} />
          <PrivateRoute path="/account" component={AccountSettings}/>
        </Switch>
      </UserContext.Provider>
    </Router>
  );
}

export default App;
